#
# Cookbook Name:: openvpn
# Recipe:: client
#
# Copyright 2013-2014, Inria
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Configures an OpenVPN client

package "openvpn" do
  action :install
end
service "openvpn" do
  action :nothing
end

# For each declared OpenVPN networks
node['openvpn']['clients'].each do |network_id,client_name|
  # We are client for the network_id network
  network = data_bag_item('openvpn', network_id)
  # We get the certificates
  client_cert = data_bag_item('certificates', client_name)
  ca_cert = data_bag_item('certificates', network['ca'])

  ##########################
  # Now we install the files

  # The root CA certificate & CRL
  file "/etc/openvpn/#{network['ca']}.crt" do
    content ca_cert['certificate']
    mode 0644
    notifies :restart, "service[openvpn]", :delayed
  end
  file "/etc/openvpn/#{network['ca']}_crl.pem" do
    content ca_cert['crl']
    mode 0644
    notifies :restart, "service[openvpn]", :delayed
  end

  # The server certificates
  file "/etc/openvpn/#{client_name}.crt" do
    content client_cert['certificate']
    mode 0644
    notifies :restart, "service[openvpn]", :delayed
  end

  file "/etc/openvpn/#{client_name}.key" do
    content `echo "#{client_cert['key']}" | openssl rsa -passin "pass:#{client_cert['pass']}" 2>/dev/null`
    mode 0600
    notifies :restart, "service[openvpn]", :delayed
  end

  ################################
  # The OpenVPN configuration
  vars = {
    "name" => network['id'],
    "port" => network['port'] ? network['port'] : 1194,
    "protocol" => network['protocol'] ? network['protocol'] : "udp",
    "ca" => network['ca'],
    "client_name" => client_name,
    "server_host" => network['host']
  }
  template "/etc/openvpn/#{network['id']}.conf" do
    source "client.conf.erb"
    variables vars
    mode 0644
    notifies :restart, "service[openvpn]", :delayed
  end
end
