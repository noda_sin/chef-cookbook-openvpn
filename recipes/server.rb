#
# Cookbook Name:: openvpn
# Recipe:: server
#
# Copyright 2013-2014, Inria
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Configures an OpenVPN client

include_recipe "iptables"

package "openvpn" do
  action :install
end
service "openvpn" do
  action :nothing
end

server_ports = []
# For each declared OpenVPN networks
node['openvpn']['servers'].each do |network_id,server_name|
  # We are server for the network_id network
  network = data_bag_item('openvpn', network_id)
  # We get the certificates
  server_cert = data_bag_item('certificates', server_name)
  ca_cert = data_bag_item('certificates', network['ca'])

  ##########################
  # Now we install the files

  # The root CA certificate & CRL
  file "/etc/openvpn/#{network['ca']}.crt" do
    content ca_cert['certificate']
    mode 0644
    notifies :restart, "service[openvpn]", :delayed
  end
  file "/etc/openvpn/#{network['ca']}_crl.pem" do
    content ca_cert['crl']
    mode 0644
    notifies :restart, "service[openvpn]", :delayed
  end

  # The server certificates
  file "/etc/openvpn/#{server_name}.crt" do
    content server_cert['certificate']
    mode 0644
    notifies :restart, "service[openvpn]", :delayed
  end

  file "/etc/openvpn/#{server_name}.key" do
    content `echo "#{server_cert['key']}" | openssl rsa -passin "pass:#{server_cert['pass']}" 2>/dev/null`
    mode 0600
    notifies :restart, "service[openvpn]", :delayed
  end

  # Diffie Helman parameters
  file "/etc/openvpn/#{network['id']}_dh.pem" do
    content network['dh']
    mode 0644
    notifies :restart, "service[openvpn]", :delayed
  end

  ################################
  # The OpenVPN configuration
  template "/etc/openvpn/#{network['id']}.conf" do
    source "server.conf.erb"
    variables({
                "name" => network['id'],
                "port" => network['port'] ? network['port'] : 1194,
                "protocol" => network['protocol'] ? network['protocol'] : "udp",
                "ca" => network['ca'],
                "server_name" => server_name,
                "ip" => network['ip'] ? network['ip'] : "10.8.0.0",
                "c2c" => network['c2c'],
		"push" => network['push']
              })
    mode 0644
    notifies :restart, "service[openvpn]", :delayed
  end

  directory "/etc/openvpn/ccd_#{network['id']}" do
    mode 0755
  end
  
  if network['servers'] then
    network['servers'].each do |name,server_ip|
      template "/etc/openvpn/ccd_#{network['id']}/#{name}" do
        source "static_ip.erb"
        variables({ "ip" => server_ip })
      end
    end
  end

  ###############################
  # IP Tables configuration
  server_ports.push({
    "port" => network['port'] ? network['port'] : 1194,
    "protocol" => network['protocol'] ? network['protocol'] : "udp"
  })
end

iptables_rule "openvpn" do
  source "iptables.conf.erb"
  variables({"ports" => server_ports})
end
