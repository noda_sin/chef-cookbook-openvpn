openvpn Cookbook
================
This cookbook contains recipes to simply cook OpenVPN clients and servers.

Requirements
------------

#### packages
- `iptables` - to configure port

Attributes
----------

#### openvpn::server

Key                        | Type    | Description                      | Default
---------------------------|---------|----------------------------------|---------
__['openvpn']['servers']__ | Hash    | The list of network to configure, each entry gives a network identifier as key and the server name as the value | `{}`

#### openvpn::client

Key                        | Type    | Description                      | Default
---------------------------|---------|----------------------------------|---------
__['openvpn']['clients']__ | Hash    | The list of network to configure, each entry gives a network identifier as key and the client name as the value | `{}`

Usage
-----
#### openvpn::default
Does nothing

#### openvpn::server
Installs and configures Open VPN service on the node.

e.g.
Include `openvpn::server` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[openvpn::server]"
  ]
}
```

It will then installs all network specified in the __['openvpn']['servers']__ hash. If we consider that each entry is of the form `network_id => server_name` then you must provides the two following data bag entries:
```json
data_bag_item('certificates', server_name):
   {
     "id": server_name,
     "certificate": "PEM CERTIFICATE",
     "key": "CERTIFICATE KEY",
     "pass": "CERTIFICATE KEY PASSWORD"
   }
```
and
```json
data_bag_item('openvpn', network_id):
   {
     "id": network_id,
     "ca": certificate_authority_name,
     "dh": "DIFFIE HELMAN PARAMETERS",
     "port": server_port,
     "protocol": ("udp"|"tcp"),
     "ip": network_ip,
     "c2c": true|false,
     "host": host,
     "push": [ push_value1, push_value2 ],
     "servers": { "host1": "ip1", "host2": "ip2" }
   }
```

The `certificates` data bag stores the certificates of the network, the entry __server_name__ is the one for the OpenVPN server and contains the certificate PEM file content, the private key and the password of the key.

The `openvpn` data bag stores the configuration of the OpenVPN servers and contains the following keys:

Key          | Type    | Description                      | Default
-------------|---------|----------------------------------|---------
__ca__       | String  | The name of the certificate authority to use, it should refers to the corresponding item in the `certificate` data bag. This item should have two keys: `certifcate` containing the public certificate and `crl` containing the authority revocation list. | **MANDATORY**
__dh__       | String  | The Diffie Helman parameters.    | **MANDATORY**
__port__     | Integer | The server port.                 | 1194
__protocol__ | String  | The protocol to use, __udp__ or __tcp__ | __udp__
__ip__       | String  | The IP of the network (/24).     | __10.8.0.0__
__c2c__      | Boolean | Does the network allow client to client communication? | __false__
__host__     | String  | IP / FQDN of the server (this node). | __node['fqdn']__
__push__     | Array   | A list of additionnal PUSH lines to add in the LDAP configuration | __[]__
__servers__  | Hash    | A list of static ip servers, the key should be the client name of the corresponding server | __{}__

#### openvpn::client
Installs and configures Open VPN client on the node.

e.g.
Include `openvpn::server` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[openvpn::client]"
  ]
}
```

It will then installs all network specified in the __['openvpn']['clients']__ hash. If we consider that each entry is of the form `network_id => client_name` then you must provides the two following data bag entries: `data_bag_item('certificates', client_name)` and `data_bag_item('openvpn', network_id)`.

The `certificates` data bag stores the certificates of the network, the entry __client_name__ is the one for the OpenVPN client and contains the certificate PEM file content, the private key and the password of the key. See the server section to get the format of this data bag.

The `openvpn` data bag stores the configuration of the OpenVPN servers. See the server section to get more information on this data bag.

License and Authors
-------------------
Licence: Apache License version 2.0
Authors:

- Damien Martin-Guillerez <damien.martin-guillerez@inria.fr>