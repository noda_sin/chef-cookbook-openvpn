name             'iqspot-vpn'
maintainer       'iQSpot'
maintainer_email 'dev@iqspot.fr'
license          'Apache 2.0'
description      'Installs/Configures Open VPN'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.0.0'
recipe            "openvpn", "Empty, use one of the other recipes"
recipe            "openvpn::client", "Install an OpenVPN client"
recipe            "openvpn::server", "Install an OpenVPN server"

%w{ ubuntu debian redhat centos amazon scientific}.each do |os|
  supports os
end

depends "iptables"
